# Expired Apt Key

The Dataspeed key expired on September 26th 2017. To renew the key, simply run the ```apt-key --recv-keys``` command from the installation procedure:

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 66F84AE1EB71A8AC108087DCAF677210FF6D3CDA
```

The renewed key will not expire.

Example output is shown below for both Ubuntu 16.04 and 14.04.

# ROS Kinetic / Ubuntu 16.04
```bash
user@ubuntu:~$ sudo apt update 
Hit:1 http://packages.dataspeedinc.com/ros/ubuntu xenial InRelease
Get:2 http://security.ubuntu.com/ubuntu xenial-security InRelease [102 kB]
Hit:3 http://us.archive.ubuntu.com/ubuntu xenial InRelease
Get:4 http://us.archive.ubuntu.com/ubuntu xenial-updates InRelease [102 kB]
Err:1 http://packages.dataspeedinc.com/ros/ubuntu xenial InRelease
  The following signatures were invalid: KEYEXPIRED 1506434891  KEYEXPIRED 1506434891  KEYEXPIRED 1506434891  KEYEXPIRED 1506434891
Hit:5 http://packages.ros.org/ros/ubuntu xenial InRelease
Get:6 http://us.archive.ubuntu.com/ubuntu xenial-backports InRelease [102 kB]
Fetched 306 kB in 0s (574 kB/s)
Reading package lists... Done
Building dependency tree
Reading state information... Done
All packages are up to date.
W: An error occurred during the signature verification. The repository is not updated and the previous index files will be used. GPG error: http://packages.dataspeedinc.com/ros/ubuntu xenial InRelease: The following signatures were invalid: KEYEXPIRED 1506434891  KEYEXPIRED 1506434891  KEYEXPIRED 1506434891  KEYEXPIRED 1506434891
W: Failed to fetch http://packages.dataspeedinc.com/ros/ubuntu/dists/xenial/InRelease  The following signatures were invalid: KEYEXPIRED 1506434891  KEYEXPIRED 1506434891  KEYEXPIRED 1506434891  KEYEXPIRED 1506434891
W: Some index files failed to download. They have been ignored, or old ones used instead.

user@ubuntu:~$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 66F84AE1EB71A8AC108087DCAF677210FF6D3CDA
Executing: /tmp/tmp.i9IbJg0dwb/gpg.1.sh --keyserver
keyserver.ubuntu.com
--recv-keys
66F84AE1EB71A8AC108087DCAF677210FF6D3CDA
gpg: requesting key FF6D3CDA from hkp server keyserver.ubuntu.com
gpg: key FF6D3CDA: "Dataspeed <info@dataspeedinc.com>" 1 new signature
gpg: Total number processed: 1
gpg:         new signatures: 1

user@ubuntu:~$ sudo apt update
Hit:1 http://us.archive.ubuntu.com/ubuntu xenial InRelease
Get:3 http://us.archive.ubuntu.com/ubuntu xenial-updates InRelease [102 kB]
Get:2 http://packages.dataspeedinc.com/ros/ubuntu xenial InRelease [1,787 B]
Hit:4 http://packages.ros.org/ros/ubuntu xenial InRelease
Hit:5 http://packages.osrfoundation.org/gazebo/ubuntu-stable xenial InRelease
Get:6 http://security.ubuntu.com/ubuntu xenial-security InRelease [102 kB]
Get:7 http://us.archive.ubuntu.com/ubuntu xenial-backports InRelease [102 kB]
Get:8 http://repo.dataspeedinc.com/ros/ubuntu xenial InRelease [1,778 B]
Get:9 http://repo.dataspeedinc.com/ros/ubuntu xenial/main amd64 Packages [656 B]
Fetched 311 kB in 20s (15.4 kB/s)
Reading package lists... Done
Building dependency tree
Reading state information... Done
All packages are up to date.
```

# ROS Indigo / Ubuntu 14.04
```bash
user@ubuntu:~$ sudo apt-get update
Get:1 http://security.ubuntu.com trusty-security InRelease [65.9 kB]
Ign http://us.archive.ubuntu.com trusty InRelease
Get:2 http://packages.dataspeedinc.com trusty InRelease [1,787 B]
Ign http://packages.dataspeedinc.com trusty InRelease
Get:3 http://us.archive.ubuntu.com trusty-updates InRelease [65.9 kB]
Ign http://packages.dataspeedinc.com trusty/main amd64 Packages/DiffIndex
Hit http://packages.ros.org trusty InRelease
Get:4 http://security.ubuntu.com trusty-security/main Sources [143 kB]
Ign http://ppa.launchpad.net trusty InRelease
Hit http://packages.ros.org trusty/main amd64 Packages
Ign http://extras.ubuntu.com trusty InRelease
Hit http://us.archive.ubuntu.com trusty-backports InRelease
Get:5 http://security.ubuntu.com trusty-security/restricted Sources [4,931 B]
Hit http://us.archive.ubuntu.com trusty Release.gpg
Hit http://packages.ros.org trusty/main i386 Packages
Get:6 http://security.ubuntu.com trusty-security/universe Sources [63.8 kB]
Get:7 http://us.archive.ubuntu.com trusty-updates/main Sources [406 kB]
Hit http://extras.ubuntu.com trusty Release.gpg
Get:8 http://security.ubuntu.com trusty-security/multiverse Sources [3,209 B]
Get:9 http://security.ubuntu.com trusty-security/main amd64 Packages [671 kB]
Hit http://extras.ubuntu.com trusty Release
Hit http://ppa.launchpad.net trusty Release.gpg
Get:10 http://security.ubuntu.com trusty-security/restricted amd64 Packages [14.0 kB]
Get:11 http://us.archive.ubuntu.com trusty-updates/restricted Sources [6,322 B]
Hit http://extras.ubuntu.com trusty/main Sources
Hit http://packages.dataspeedinc.com trusty/main amd64 Packages
Get:12 http://security.ubuntu.com trusty-security/universe amd64 Packages [187 kB]
Get:13 http://us.archive.ubuntu.com trusty-updates/universe Sources [190 kB]
Hit http://extras.ubuntu.com trusty/main amd64 Packages
Hit http://ppa.launchpad.net trusty Release
Get:14 http://us.archive.ubuntu.com trusty-updates/multiverse Sources [7,749 B]
Hit http://extras.ubuntu.com trusty/main i386 Packages
Get:15 http://us.archive.ubuntu.com trusty-updates/main amd64 Packages [1,025 kB]
Ign http://packages.dataspeedinc.com trusty/main Translation-en_US
Ign http://packages.dataspeedinc.com trusty/main Translation-en
Ign http://packages.ros.org trusty/main Translation-en_US
Get:16 http://security.ubuntu.com trusty-security/multiverse amd64 Packages [4,116 B]
Hit http://ppa.launchpad.net trusty/main amd64 Packages
Get:17 http://security.ubuntu.com trusty-security/main i386 Packages [617 kB]
Ign http://packages.ros.org trusty/main Translation-en
Get:18 http://security.ubuntu.com trusty-security/restricted i386 Packages [13.7 kB]
Hit http://ppa.launchpad.net trusty/main i386 Packages
Get:19 http://security.ubuntu.com trusty-security/universe i386 Packages [187 kB]
Get:20 http://security.ubuntu.com trusty-security/multiverse i386 Packages [4,276 B]
Hit http://security.ubuntu.com trusty-security/main Translation-en
Hit http://security.ubuntu.com trusty-security/multiverse Translation-en
Hit http://ppa.launchpad.net trusty/main Translation-en
Hit http://security.ubuntu.com trusty-security/restricted Translation-en
Hit http://security.ubuntu.com trusty-security/universe Translation-en
Get:21 http://us.archive.ubuntu.com trusty-updates/restricted amd64 Packages [17.1 kB]
Get:22 http://us.archive.ubuntu.com trusty-updates/universe amd64 Packages [427 kB]
Ign http://extras.ubuntu.com trusty/main Translation-en_US
Ign http://extras.ubuntu.com trusty/main Translation-en
Get:23 http://us.archive.ubuntu.com trusty-updates/multiverse amd64 Packages [14.3 kB]
Get:24 http://us.archive.ubuntu.com trusty-updates/main i386 Packages [972 kB]
Get:25 http://us.archive.ubuntu.com trusty-updates/restricted i386 Packages [16.8 kB]
Get:26 http://us.archive.ubuntu.com trusty-updates/universe i386 Packages [429 kB]
Get:27 http://us.archive.ubuntu.com trusty-updates/multiverse i386 Packages [14.7 kB]
Hit http://us.archive.ubuntu.com trusty-updates/main Translation-en
Hit http://us.archive.ubuntu.com trusty-updates/multiverse Translation-en
Hit http://us.archive.ubuntu.com trusty-updates/restricted Translation-en
Hit http://us.archive.ubuntu.com trusty-updates/universe Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/main Sources
Hit http://us.archive.ubuntu.com trusty-backports/restricted Sources
Hit http://us.archive.ubuntu.com trusty-backports/universe Sources
Hit http://us.archive.ubuntu.com trusty-backports/multiverse Sources
Hit http://us.archive.ubuntu.com trusty-backports/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/restricted amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/universe amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/multiverse amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/main i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/restricted i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/universe i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/multiverse i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/main Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/multiverse Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/restricted Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/universe Translation-en
Hit http://us.archive.ubuntu.com trusty Release
Hit http://us.archive.ubuntu.com trusty/main Sources
Hit http://us.archive.ubuntu.com trusty/restricted Sources
Hit http://us.archive.ubuntu.com trusty/universe Sources
Hit http://us.archive.ubuntu.com trusty/multiverse Sources
Hit http://us.archive.ubuntu.com trusty/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty/restricted amd64 Packages
Hit http://us.archive.ubuntu.com trusty/universe amd64 Packages
Hit http://us.archive.ubuntu.com trusty/multiverse amd64 Packages
Hit http://us.archive.ubuntu.com trusty/main i386 Packages
Hit http://us.archive.ubuntu.com trusty/restricted i386 Packages
Hit http://us.archive.ubuntu.com trusty/universe i386 Packages
Hit http://us.archive.ubuntu.com trusty/multiverse i386 Packages
Hit http://us.archive.ubuntu.com trusty/main Translation-en
Hit http://us.archive.ubuntu.com trusty/multiverse Translation-en
Hit http://us.archive.ubuntu.com trusty/restricted Translation-en
Hit http://us.archive.ubuntu.com trusty/universe Translation-en
Ign http://us.archive.ubuntu.com trusty/main Translation-en_US
Ign http://us.archive.ubuntu.com trusty/multiverse Translation-en_US
Ign http://us.archive.ubuntu.com trusty/restricted Translation-en_US
Ign http://us.archive.ubuntu.com trusty/universe Translation-en_US
Fetched 5,573 kB in 6s (900 kB/s)
Reading package lists... Done
W: GPG error: http://packages.dataspeedinc.com trusty InRelease: The following signatures were invalid: KEYEXPIRED 1506434891 KEYEXPIRED 1506434891 KEYEXPIRED 1506434891 KEYEXPIRED 1506434891

user@ubuntu:~$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 66F84AE1EB71A8AC108087DCAF677210FF6D3CDA
Executing: gpg --ignore-time-conflict --no-options --no-default-keyring --homedir /tmp/tmp.NRJMhKLwNN --no-auto-check-trustdb --trust-model always --keyring /etc/apt/trusted.gpg --primary-keyring /etc/apt/trusted.gpg --keyserver keyserver.ubuntu.com --recv-keys 66F84AE1EB71A8AC108087DCAF677210FF6D3CDA
gpg: requesting key FF6D3CDA from hkp server keyserver.ubuntu.com
gpg: key FF6D3CDA: "Dataspeed <info@dataspeedinc.com>" 1 new signature
gpg: Total number processed: 1
gpg:         new signatures: 1

user@ubuntu:~$ sudo apt-get update
Get:1 http://packages.dataspeedinc.com trusty InRelease [1,787 B]
Ign http://us.archive.ubuntu.com trusty InRelease
Hit http://security.ubuntu.com trusty-security InRelease
Get:2 http://packages.dataspeedinc.com trusty/main amd64 Packages [10.4 kB]
Hit http://us.archive.ubuntu.com trusty-updates InRelease
Hit http://packages.ros.org trusty InRelease
Hit http://security.ubuntu.com trusty-security/main Sources
Hit http://us.archive.ubuntu.com trusty-backports InRelease
Hit http://security.ubuntu.com trusty-security/restricted Sources
Hit http://us.archive.ubuntu.com trusty Release.gpg
Ign http://ppa.launchpad.net trusty InRelease
Hit http://packages.ros.org trusty/main amd64 Packages
Hit http://security.ubuntu.com trusty-security/universe Sources
Hit http://us.archive.ubuntu.com trusty-updates/main Sources
Ign http://extras.ubuntu.com trusty InRelease
Hit http://security.ubuntu.com trusty-security/multiverse Sources
Hit http://us.archive.ubuntu.com trusty-updates/restricted Sources
Hit http://packages.ros.org trusty/main i386 Packages
Hit http://security.ubuntu.com trusty-security/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty-updates/universe Sources
Hit http://us.archive.ubuntu.com trusty-updates/multiverse Sources
Hit http://security.ubuntu.com trusty-security/restricted amd64 Packages
Hit http://us.archive.ubuntu.com trusty-updates/main amd64 Packages
Hit http://security.ubuntu.com trusty-security/universe amd64 Packages
Hit http://extras.ubuntu.com trusty Release.gpg
Hit http://us.archive.ubuntu.com trusty-updates/restricted amd64 Packages
Hit http://security.ubuntu.com trusty-security/multiverse amd64 Packages
Ign http://packages.dataspeedinc.com trusty/main Translation-en_US
Hit http://ppa.launchpad.net trusty Release.gpg
Hit http://us.archive.ubuntu.com trusty-updates/universe amd64 Packages
Hit http://security.ubuntu.com trusty-security/main i386 Packages
Ign http://packages.dataspeedinc.com trusty/main Translation-en
Hit http://us.archive.ubuntu.com trusty-updates/multiverse amd64 Packages
Hit http://security.ubuntu.com trusty-security/restricted i386 Packages
Hit http://extras.ubuntu.com trusty Release
Hit http://us.archive.ubuntu.com trusty-updates/main i386 Packages
Hit http://security.ubuntu.com trusty-security/universe i386 Packages
Hit http://us.archive.ubuntu.com trusty-updates/restricted i386 Packages
Hit http://security.ubuntu.com trusty-security/multiverse i386 Packages
Hit http://us.archive.ubuntu.com trusty-updates/universe i386 Packages
Hit http://security.ubuntu.com trusty-security/main Translation-en
Hit http://us.archive.ubuntu.com trusty-updates/multiverse i386 Packages
Hit http://security.ubuntu.com trusty-security/multiverse Translation-en
Hit http://extras.ubuntu.com trusty/main Sources
Hit http://us.archive.ubuntu.com trusty-updates/main Translation-en
Hit http://security.ubuntu.com trusty-security/restricted Translation-en
Hit http://ppa.launchpad.net trusty Release
Hit http://us.archive.ubuntu.com trusty-updates/multiverse Translation-en
Hit http://security.ubuntu.com trusty-security/universe Translation-en
Hit http://extras.ubuntu.com trusty/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty-updates/restricted Translation-en
Hit http://us.archive.ubuntu.com trusty-updates/universe Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/main Sources
Hit http://extras.ubuntu.com trusty/main i386 Packages
Ign http://packages.ros.org trusty/main Translation-en_US
Hit http://us.archive.ubuntu.com trusty-backports/restricted Sources
Hit http://ppa.launchpad.net trusty/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/universe Sources
Ign http://packages.ros.org trusty/main Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/multiverse Sources
Hit http://us.archive.ubuntu.com trusty-backports/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/restricted amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/universe amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/multiverse amd64 Packages
Hit http://us.archive.ubuntu.com trusty-backports/main i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/restricted i386 Packages
Hit http://ppa.launchpad.net trusty/main i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/universe i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/multiverse i386 Packages
Hit http://us.archive.ubuntu.com trusty-backports/main Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/multiverse Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/restricted Translation-en
Hit http://ppa.launchpad.net trusty/main Translation-en
Hit http://us.archive.ubuntu.com trusty-backports/universe Translation-en
Hit http://us.archive.ubuntu.com trusty Release
Hit http://us.archive.ubuntu.com trusty/main Sources
Hit http://us.archive.ubuntu.com trusty/restricted Sources
Hit http://us.archive.ubuntu.com trusty/universe Sources
Hit http://us.archive.ubuntu.com trusty/multiverse Sources
Hit http://us.archive.ubuntu.com trusty/main amd64 Packages
Hit http://us.archive.ubuntu.com trusty/restricted amd64 Packages
Hit http://us.archive.ubuntu.com trusty/universe amd64 Packages
Hit http://us.archive.ubuntu.com trusty/multiverse amd64 Packages
Hit http://us.archive.ubuntu.com trusty/main i386 Packages
Hit http://us.archive.ubuntu.com trusty/restricted i386 Packages
Hit http://us.archive.ubuntu.com trusty/universe i386 Packages
Hit http://us.archive.ubuntu.com trusty/multiverse i386 Packages
Hit http://us.archive.ubuntu.com trusty/main Translation-en
Ign http://extras.ubuntu.com trusty/main Translation-en_US
Hit http://us.archive.ubuntu.com trusty/multiverse Translation-en
Ign http://extras.ubuntu.com trusty/main Translation-en
Hit http://us.archive.ubuntu.com trusty/restricted Translation-en
Hit http://us.archive.ubuntu.com trusty/universe Translation-en
Ign http://us.archive.ubuntu.com trusty/main Translation-en_US
Ign http://us.archive.ubuntu.com trusty/multiverse Translation-en_US
Ign http://us.archive.ubuntu.com trusty/restricted Translation-en_US
Ign http://us.archive.ubuntu.com trusty/universe Translation-en_US
Fetched 12.2 kB in 3s (3,384 B/s)
Reading package lists... Done
```

