# Dataspeed ROS Binaries

Many Dataspeed ROS packages are not built by the [Official ROS Buildfarm](http://wiki.ros.org/buildfarm), and are not available for installation with apt-get on [packages.ros.org](http://packages.ros.org/).  
Dataspeed ROS packages are available from a separate apt-get server: [packages.dataspeedinc.com](http://packages.dataspeedinc.com/).

## Supported Platforms

ROS Distributions: [Jazzy](https://docs.ros.org/en/jazzy/)/[Humble](https://docs.ros.org/en/humble/)/[Foxy](https://docs.ros.org/en/foxy/)/[Noetic](http://wiki.ros.org/noetic)/[Melodic](http://wiki.ros.org/melodic)  
Ubuntu Distributions: [Noble](http://releases.ubuntu.com/24.04/)/[Jammy](http://releases.ubuntu.com/22.04/)/[Focal](http://releases.ubuntu.com/20.04/)/[Bionic](http://releases.ubuntu.com/18.04/)  
Architectures: amd64  

## Package List

* List of ROS1 [debian packages](http://packages.dataspeedinc.com/ros/ubuntu/pool/main/r/)
* List of ROS2 [debian packages](http://packages.dataspeedinc.com/ros2/ubuntu/pool/main/r/)
* Non-Dataspeed packages:
    * ROS Kinetic: Various unreleased packages: [baxter](http://wiki.ros.org/baxter_sdk) and others
* Source code location and tags for [Jazzy](rosinstall/jazzy.rosinstall)/[Humble](rosinstall/humble.rosinstall)/[Foxy](rosinstall/foxy.rosinstall)/[Noetic](rosinstall/noetic.rosinstall)/[Melodic](rosinstall/melodic.rosinstall) (not all source code is public)
* History of debian package uploads for [ROS1](http://packages.dataspeedinc.com/ros/uploads/)/[ROS2](http://packages.dataspeedinc.com/ros2/uploads/)


## Setup apt (ROS1)

Setup your computer to accept software from the Dataspeed server.

```sh
sudo apt install curl lsb-release
curl -s https://bitbucket.org/DataspeedInc/ros_binaries/raw/master/dataspeed.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://packages.dataspeedinc.com/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-dataspeed-public.list'
sudo apt update
```

## Setup apt (ROS2)

Setup your computer to accept software from the Dataspeed server.

```sh
sudo apt install curl
sudo curl -sSL https://bitbucket.org/DataspeedInc/ros_binaries/raw/master/dataspeed.key -o /usr/share/keyrings/dataspeed-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/dataspeed-archive-keyring.gpg] http://packages.dataspeedinc.com/ros2/ubuntu $(source /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2-dataspeed-public.list > /dev/null
sudo apt update
```

## Setup rosdep (ROS1)

Make rosdep aware of Dataspeed packages.

```sh
sudo sh -c 'echo "yaml http://packages.dataspeedinc.com/ros/ros-public-'$ROS_DISTRO'.yaml '$ROS_DISTRO'" > /etc/ros/rosdep/sources.list.d/30-dataspeed-public-'$ROS_DISTRO'.list'
rosdep update
```

## Setup rosdep (ROS2)

Make rosdep aware of Dataspeed packages.

```sh
sudo sh -c 'echo "yaml http://packages.dataspeedinc.com/ros2/ros-public-'$ROS_DISTRO'.yaml '$ROS_DISTRO'" > /etc/ros/rosdep/sources.list.d/30-dataspeed-public-'$ROS_DISTRO'.list'
rosdep update
```

## Install Packages

```sh
sudo apt-get install ros-$ROS_DISTRO-dbw-mkz
sudo apt-get install ros-$ROS_DISTRO-dataspeed-boot-usb
sudo apt-get install ros-$ROS_DISTRO-dataspeed-dbw-simulator
sudo apt-get install ros-$ROS_DISTRO-velodyne
```

## Updates

```sh
sudo apt update && sudo apt upgrade && rosdep update
```

# Troubleshooting

* Error: ```KEYEXPIRED 1506434891```
    * The key has expired and must be renewed. See [expired_key](expired_key.md) for more details.

